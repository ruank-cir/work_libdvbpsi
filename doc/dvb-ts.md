一些网址：

[一些dvb-ts流的样本包](https://ccextractor.org/public/general/tvsamples/)

[dvb源码分析、官网](https://blog.csdn.net/weixin_34352449/article/details/94453898?spm=1001.2101.3001.6650.5&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-5-94453898-blog-77512233.235%5Ev38%5Epc_relevant_anti_t3_base&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-5-94453898-blog-77512233.235%5Ev38%5Epc_relevant_anti_t3_base&utm_relevant_index=8)

[dvb中ts流解析](https://blog.csdn.net/yexiangCSDN/article/details/79853824)

[PAT和PMT分析](https://blog.csdn.net/yexiangCSDN/article/details/79854394)

[解析ts示例](https://blog.csdn.net/yexiangCSDN/article/details/79862434)

[ts传输解析制作](https://www.onelib.biz/doc/stb/appendix/ts.html)

[ts流、包结构以及同步](https://www.cnblogs.com/doscho/p/7678579.html)

[libdvbpsi接口手册](https://videolan.videolan.me/libdvbpsi/usage.html)

[dvb-ts流解析](https://blog.csdn.net/yexiangCSDN/article/details/79853824)

[ts流解析，主要是扩展字段的解析](https://zhuanlan.zhihu.com/p/608579756)

[ts中pat\pmt\sdt解析](https://www.cnblogs.com/lifan3a/articles/6214429.html)

[ts流中sdt表](https://blog.csdn.net/hejinjing_tom_com/article/details/127289558?spm=1001.2101.3001.6650.3&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-3-127289558-blog-105807541.235%5Ev38%5Epc_relevant_anti_t3_base&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-3-127289558-blog-105807541.235%5Ev38%5Epc_relevant_anti_t3_base&utm_relevant_index=4)

[SDT, EIT, TDT,TOT](https://blog.csdn.net/chilv/article/details/108730871?spm=1001.2101.3001.6650.1&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-1-108730871-blog-127289558.235%5Ev38%5Epc_relevant_anti_t3_base&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-1-108730871-blog-127289558.235%5Ev38%5Epc_relevant_anti_t3_base&utm_relevant_index=2)

[psi信息解析](https://blog.csdn.net/u013898698/article/details/79550395)



当使用DVB传输流（DVB-TS）时，会使用多种表格来传递信息。以下是各个表格的详细信息：

1. PAT（Program Association Table）：PAT提供了一个映射表，它将每个程序号（PID）与其所属的节目号关联起来。PAT通常有一个固定的PID（0x0000），并且每个DVB-TS数据包都可以携带PAT表格。PAT表格的作用是为了查找PMT表格，以获取特定节目的所有相关信息。

   ```c
   PAT表格定义如下：
   
   typedef struct TS_PAT_Program
   {
    unsigned program_number    :16; //节目号
    unsigned program_map_PID   :13;   //节目映射表的PID，节目号大于0时对应的PID，每个节目对应一个
   }TS_PAT_Program;
   
   //PAT表结构体
   typedef struct TS_PAT
   {
       unsigned table_id                        : 8; //固定为0x00 ，标志是该表是PAT
       unsigned section_syntax_indicator        : 1; //段语法标志位，固定为1
       unsigned zero                            : 1; //0
       unsigned reserved_1                        : 2; // 保留位
       unsigned section_length                    : 12; //表示这个字节后面有用的字节数，包括CRC32
       unsigned transport_stream_id            : 16; //该传输流的ID，区别于一个网络中其它多路复用的流
       unsigned reserved_2                        : 2;// 保留位
       unsigned version_number                    : 5; //范围0-31，表示PAT的版本号
       unsigned current_next_indicator            : 1; //发送的PAT是当前有效还是下一个PAT有效
       unsigned section_number                    : 8; //分段的号码。PAT可能分为多段传输，第一段为00，以后每个分段加1，最多可能有256个分段
       unsigned last_section_number            : 8;  //最后一个分段的号码
    
    std::vector<TS_PAT_Program> program;
       unsigned reserved_3                        : 3; // 保留位
       unsigned network_PID                    : 13; //网络信息表（NIT）的PID,节目号为0时对应的PID为network_PID
   
       unsigned CRC_32                            : 32;  //CRC32校验码
   } TS_PAT;
   
   /*
   PAT语法字段介绍：
   
   table_id: 这是一个1B的字段，该字段标识了一个TS PSI分段的内容是节目关联分段，条件访问分段还是TS节目映射分段等。对于PAT，置为0x0。
   
   section_syntax_indicator: 这是一个1bit的字段，对于PAT，置为0x1.
   
   section_length:这是一个12bit的字段，该字段指示分段的字节数，由分段长的字段开始，包含CRC，其值不超过1021。
   
   Transport_stream_id: 这是一个2个B的字段，作为一个标签，该字段指出在网络中与其他复用流的区别标志，其值由用户定义。
   
   version_number: 这是一个5b字段，该字段指出所有的PAT的版本号。一旦PAT有变化，版本号加1，当增加到31时，版本号循环回到0。
   
   current_next_indicator:这个1b指示位，置为1时，表示传送的PAT当前可以使用；置为0时，表示该传送的表不能使用，下一个表变为有效。
   
   section_number:这是1B字段，给出拉该分段的数目。在PAT中的第一个分段的section_number位0x00,PAT中每一个分段将加1。 
   
   last_section_number:这时一个1B字段，该字段指出了最后一个分段号。在整个PAT中即分段号的最大数目。 
   
   program_number:这是一个2B字段，该字段指出了节目对于哪一个program_map_PID是可以使用的。如果是0x0000，那么后面的PID是网络PID，则其他值由用户定义。 
   
   network_PID:这是一个13b字段，该字段指出含有NIT的TS包的PID值。此值由用户定义。
   
   Program_map_PID: 这是一个13b字段，该字段知道TS包中的PID值。该TS应包含适用于program_number所指明的节目的program_map_section，该字段由节目号指定。一个节目号有一个progarm_map_PID的定义。该字段由用户定义。 
   
   CRC_32:这是一个32b字段，用来检验数据正确性的循环冗余校验码。 
   */
   ```

   

2. PMT（Program Map Table）：PMT提供了特定节目的音频、视频和其他元素的描述。PMT包含有关使用的编解码器、音频采样率、视频分辨率等信息，并将这些信息映射到相应的PID上。根据需要，每个节目通常都有一个对应的PMT表格。PMT表格的PID由PAT表格中的条目确定。

   ```c
   PAT表格定义如下：
   
   typedef struct TS_PAT_Program
   {
    unsigned program_number    :16; //节目号
    unsigned program_map_PID   :13;   //节目映射表的PID，节目号大于0时对应的PID，每个节目对应一个
   }TS_PAT_Program;
   
   //PAT表结构体
   typedef struct TS_PAT
   {
       unsigned table_id                        : 8; //固定为0x00 ，标志是该表是PAT
       unsigned section_syntax_indicator        : 1; //段语法标志位，固定为1
       unsigned zero                            : 1; //0
       unsigned reserved_1                        : 2; // 保留位
       unsigned section_length                    : 12; //表示这个字节后面有用的字节数，包括CRC32
       unsigned transport_stream_id            : 16; //该传输流的ID，区别于一个网络中其它多路复用的流
       unsigned reserved_2                        : 2;// 保留位
       unsigned version_number                    : 5; //范围0-31，表示PAT的版本号
       unsigned current_next_indicator            : 1; //发送的PAT是当前有效还是下一个PAT有效
       unsigned section_number                    : 8; //分段的号码。PAT可能分为多段传输，第一段为00，以后每个分段加1，最多可能有256个分段
       unsigned last_section_number            : 8;  //最后一个分段的号码
    
    std::vector<TS_PAT_Program> program;
       unsigned reserved_3                        : 3; // 保留位
       unsigned network_PID                    : 13; //网络信息表（NIT）的PID,节目号为0时对应的PID为network_PID
   
       unsigned CRC_32                            : 32;  //CRC32校验码
   } TS_PAT;
   
   /*
   PMT语法字段介绍：
   
   table_id:这是一个1B的字段,对于PMT置为0x02。 
   
   section_syntax_indicatior:这是一个1b的字段，对于PMT，值为1。
   
   section_length: 这是一个12b的字段，前两位置为00，表示字段的字节数，包括section_length字段和CRC字段。
   
   program_number:该字段指出该节目对应于可应用的program_map_PID。一个节目定义仅含一个TS的program_map_section，因此一个节目定义长度不超过1026B。
   
   version_numbe:这是一个5b的字段，该字段指出了program_map_section的版本号。当字段中有关信息发生变化时，版本号将以32为模加1。版本号时关于一个节目的定义，因此版本号是关于单一段的定义。 
   
   curren_next_indicator:这是一个1b的字段，当字段置为0时，表示当前传送的program_map_section可以；当置为0时，表示当前传送的program_map_section不可用，下一个TS的program_map_section有效。
   
   section_number:这是一个1B长度的字段，该字段值总是0x00。 
   
   last_section_number:这是一个1B长度的字段，该字段值总是0x00。
   
   PCR_PID:这是一个13b长度的字段，该字段指示TS包的PID值。该TS包含有PCR字段，而该PCR值对应于由节目号指定的节目。如果对于私有数据流的急忙定义与PCR无关，该字段的值将位0x1FFF。 
   
   program_info_length:这是一个12b长的字段，前2位是00。该字段指出跟随其后对节目信息描述的字节数。
   
   stream_type:这是一个1B长度的字段，该字段指定特定PID的节目元素包的类型。即他定义了在TS包中PES流的类型,如下表2.4所示。该处的PID由element_PID指定。
   
   element_PID:这是一个13b长度的字段。该字段指示TS包的PID值。这些TS包含有相关的节目元素。 
   
   ES_info_length:这是一个12b长度的字段，前2位为00。该字段指示跟随其后的描述相关节目元素的字节数。
   
   CRC_32:这是一个32b字段，用来检验数据正确性的循环冗余校验码。
   */
   ```

   

3. CAT（Conditional Access Table）：CAT提供了有关访问控制系统（CAS）和加密方案的信息。CAT表格通常有一个固定的PID（0x0001），并且每个DVB-TS数据包都可以携带CAT表格。

4. SDT（Service Description Table）：SDT提供了有关已广播服务的详细信息。例如，它可能包括频道名称、类型、位置和其他元数据。SDT表格通常有一个固定的PID（0x0011），并且每个DVB-TS数据包都可以携带SDT表格。

   ```c
   
   /// sdt 表结构
   Syntax（句法结构）         No. ofbits（所占位数）   Identifier(识别符)  Note(注释)  
   service_description_section(){
       table_id                     8         uimsbf                                                  
       Section_syntax_indicator       1    bslbf                   通常设为“1
       Reserved_future_use        1            bslbf
       Reserved                     2        bslbf
       Section_length                12          uimsbf                 见注释
       transport_stream_id            16          uimsbf                给出TS识别号
       Reserved                              2           bslbf
       Version_number                    5             uimsbf             见注释
       Current_next_indicator        1               bslbf                 见注释
       Section_number      8    uimsbf            见注释
       last_section_number      8     uimsbf            见注释
       original_nerwork_id      16      uimsbf            见注释
       reserved_future_use    8     bslbf
       for(i=0;i<N;i++){
           service_id        16 uimsbf           见注释
           reserved_future_use    6    bslbf
           EIT_schedule_flag  1  bslbf            见注释
           EIT_present_following_flag    1             bslbf            见注释
           running_status    3        uimsbf        见下面分析
           freed_CA_mode        1      bslbf            见注释
           descriptors_loop_length     12                      uimsbf
           for(j=0;j<N;j++){
               descriptor()
           }
       }
       CRC_32     32                  rpchof                  见注释
   }
   
   //	service_descriptor
   service_descriptor(){
   
       descriptor_tag                       8                       uimsbf
   
       descriptor_length                   8                       uimsbf
   
       service_type                          8                      uimsbf
   
       service_provider_name_length  	8                    uimsbf
   
       for(i=0;i<N;i++){
   
           Char                                 8                      uimsbf
   
       }
   
       service_name_length            8                      uimsbf
   
       for(i=0;i<N;i++){
   				// 描述
           Char                                8                      uimsbf
   
       }
   
   }
   ```

   - ***service_type\***（业务类型）：这里的业务类型和NIT中的业务列表描述符中的service_type编码是一致的：

     | service_type | 描述                    |
     | ------------ | ----------------------- |
     | 0x00         | 预留使用                |
     | 0x01         | 数字电视业务            |
     | 0x02         | 数字音频广播业务        |
     | 0x03         | 图文电视业务            |
     | 0x04         | NVOD参考业务            |
     | 0x05         | NVOD时移业务            |
     | 0x06         | 马赛克业务              |
     | 0x07         | PAL制编码信号           |
     | 0x08         | SECAM制编码信号         |
     | 0x09         | D/D2-MAC                |
     | 0x0A         | 调频广播                |
     | 0x0B         | NTSC制信号              |
     | 0x0C         | 数据广播业务            |
     | 0x0D         | 公共接口使用预留        |
     | 0x0E         | RCS映射（见EN 301 790） |
     | 0x0F         | RCS FLS（见EN 301 790） |
     | 0x10         | DVB MHP业务             |
     | 0x11~0x7F    | 预留使用                |
     | 0x80~0xFE    | 用户定义                |
     | 0xFF         | 预留使用                |

   - runing_statu取值
   
     | 值   | 含义                 |
     | ---- | -------------------- |
     | 0    | 未定义               |
     | 1    | 未运行               |
     | 2    | 几秒后开始（如录像） |
     | 3    | 暂停                 |
     | 4    | 运行                 |
     | 5~7  | 预留                 |
- 业务描述符是SDT中最重要的描述符，也是运营商中必须的描述符。其中的service_type描述这个业务类型的域；service_name描述频道名。

5. NIT（Network Information Table）：NIT提供了有关广播网络和其所属区域的详细信息。例如，它可能包括频道列表、地理位置、可用服务等。NIT表格通常有一个固定的PID（0x0010），并且每个DVB-TS数据包都可以携带NIT表格。

   ```c
   /*
   PMT语法字段介绍：
   
   table_id:这是一个1B的字段,对于PMT置为0x02。 
   
   section_syntax_indicatior:这是一个1b的字段，对于PMT，值为1。
   
   section_length: 这是一个12b的字段，前两位置为00，表示字段的字节数，包括section_length字段和CRC字段。
   
   program_number:该字段指出该节目对应于可应用的program_map_PID。一个节目定义仅含一个TS的program_map_section，因此一个节目定义长度不超过1026B。
   
   version_numbe:这是一个5b的字段，该字段指出了program_map_section的版本号。当字段中有关信息发生变化时，版本号将以32为模加1。版本号时关于一个节目的定义，因此版本号是关于单一段的定义。 
   
   curren_next_indicator:这是一个1b的字段，当字段置为0时，表示当前传送的program_map_section可以；当置为0时，表示当前传送的program_map_section不可用，下一个TS的program_map_section有效。
   
   section_number:这是一个1B长度的字段，该字段值总是0x00。 
   
   last_section_number:这是一个1B长度的字段，该字段值总是0x00。
   
   PCR_PID:这是一个13b长度的字段，该字段指示TS包的PID值。该TS包含有PCR字段，而该PCR值对应于由节目号指定的节目。如果对于私有数据流的急忙定义与PCR无关，该字段的值将位0x1FFF。 
   
   program_info_length:这是一个12b长的字段，前2位是00。该字段指出跟随其后对节目信息描述的字节数。
   
   stream_type:这是一个1B长度的字段，该字段指定特定PID的节目元素包的类型。即他定义了在TS包中PES流的类型,如下表2.4所示。该处的PID由element_PID指定。
   
   element_PID:这是一个13b长度的字段。该字段指示TS包的PID值。这些TS包含有相关的节目元素。 
   
   ES_info_length:这是一个12b长度的字段，前2位为00。该字段指示跟随其后的描述相关节目元素的字节数。
   
   CRC_32:这是一个32b字段，用来检验数据正确性的循环冗余校验码。
    */
   ```

   

6. RST（Running Status Table）：RST提供了有关正在播放节目的状态信息。例如，它可能包括当前的时间戳、节目名称、已播放时长等。RST表格可以随时插入到DVB-TS数据流中。

7. EIT表：

   ```c
   Syntax（句法结构）         No. ofbits（所占位数）   Identifier(识别符)  Note(注释)
   
   event_information_section(){
   
   table_id                                            8                      uimsbf                                                  
   
   Section_syntax_indicator               1                          bslbf                   通常设为“1”
   
   Reserved_future_use                      1                        bslbf
   
   Reserved                                         2                        bslbf
   
   Section_length                                12                      uimsbf                 见注释
   
   service_id                                        16                       uimsbf                与PAT中的program_number一致
   
   Reserved                                         2                         bslbf
   
   Version_number                              5                         uimsbf             见注释
   
   Current_next_indicator                   1                          bslbf                 见注释
   
   Section_number                              8                          uimsbf            见注释
   
   last_section_number                       8                          uimsbf            见注释
   
   transport_stream_id                       16                         uimsbf            见注释
   
   original_nerwork_id                        16                         uimsbf            见注释
   
   segment_last_section_number     8                           uimsbf            见注释
   
   last_table_id                                     8                           uimsbf            见注释
   
   for(i=0;i<N;i++){
   
       event_id      16                         uimsbf          事件（节目）id
   
       start_time    40                         bslbf           事件（节目）开始时间
   
       duration  24                         bslbf           事件（节目）持续始时间
   
       running_status    3                         uimsbf         见注释
   
       freed_CA_mode   1                         bslbf            见注释
   
       descriptors_loop_length 12                      uimsbf
   
       for(j=0;j<N;j++){
           descriptor()
       }
   
   }
   
   CRC_32    32                  rpchof                  见注释
   
   }
   
   
   
   // short_event_descriptor
   short_event_descriptor(){
   
   descriptor_tag                       8                       uimsbf
   
   descriptor_length                   8                       uimsbf
   
     //（ISO 639-2语言代码）：24位字段，指明后续文本字段的语言。该字段包含一个由ISO 639-2定义的3字符代码。ISO 639-2/B和ISO 639-2/T均可使用。每个字符按照GB/T15273.1-1994编码为8位，并依次插入24位字段。如：法语的3字符代码“fre”，可编码为：“0110 01100111 0010 0110 0101”。
   ISO_639_language_code      24                      bslbf
   
   event_name_length               8                       uimsbf
   
   for(i=0;i<event_name_length;i++){
   
     // （事件名称字符）：一个字符串给出事件的名字。
       event_name_char             8                       uimsbf
   
   }
   
   text_length                          8                       uimsbf
   
   for(i=0;i<text_length;i++){
   // （文本字符）：一个字符串给出事件的文本描述。
       text_char                        8                       uimsbf
   
   }
   
   }
   
   
   //   扩展事件描述符（Extended_Event_Descriptor）拓展事件描述符给出了一个事件的详细文本描述。如果一个事件的信息长度超过256字节，可以使用多于一个相关联的扩展事件描述符来描述。文本信息可以分为两个栏目，一栏为条目的描述，另一栏为条目的内容。
   extended_event_descriptor(){
   
   descriptor_tag                         8           uimsbf        
   
   descriptor_length                 8           uimsbf   
   
   descriptor_number                      4             uimsbf
   
   last_descriptor_number                4             uimsbf
   
   ISO_639_language_code           24           bslbf 
   
   length_of_items                            8             uimsbf
   
   for(i=0;i<N;i++){
   
       item_description_length          8               uimsbf
   
       for(j=0;j<N;j++){
   
           item_description_char       8                uimsbf
   
       }
   
       item_length
   
       for(j=0;j<N;j++){
   
           item_char
   
       }
   
   }   
   
   text_length
   
   for(i-0;i<N;i++){
   
       text_char
   
   }             
   
   }
   
   
   
   //   内容描述符的目的是为事件提供清晰的信息描述符。根据这个描述符的信息，接收机可以清晰地知道事件的分类，并告知观众。下面给出了内容描述符的结构：
   content_descriptor(){
   
   descriptor_tag                         8           uimsbf     
   
   descriptor_length                 8           uimsbf                        
   
   for(i=0;i<N;i++){
   
       content_nibble_level1       4              uimsbf      
   
       content_nibble_level2      4              uimsbf      
   
       user_nibble                         4             uimsbf      
   
       user_nibble                         4             uimsbf      
   
   }
   
   }
   //content_nibble_level1和content_nibble_level2：根据EN 300 468V1.3.1(1998-02)中的Table 18可以确定该节目的具体分类。
   ```

   - 对于不同的事件描述符通过描述标记来区分

     ```c
     // 解析 EIT 表格数据
     void parse_eit_table(unsigned char *data, int data_len)
     {
         // 解析 EIT 表头部
         unsigned char *p = data;
         eit_header_t header = { 0 };
         header.table_id = p[0];
         header.section_syntax_indicator = (p[1] >> 7) & 0x01;
         header.section_length = ((p[1] & 0x0F) << 8) | p[2];
         header.service_id = (p[3] << 8) | p[4];
         header.version_number = (p[5] >> 1) & 0x1F;
         header.current_next_indicator = p[5] & 0x01;
         header.section_number = p[6];
         header.last_section_number = p[7];
         header.transport_stream_id = (p[8] << 8) | p[9];
         header.original_network_id = (p[10] << 8) | p[11];
         header.segment_last_section_number = p[12];
         header.last_table_id = p[13];
         
         // 解析 EIT 表格事件
         unsigned char *events_data = p + 14;
         int events_len = data_len - 14 - 4;  // 去掉表头和 CRC32
         for (int i = 0; i < events_len;) {
             eit_event_t event = { 0 };
             
             event.event_id = read_uint(events_data + i, 2);
             event.start_time = ((unsigned long long)read_uint(events_data + i + 2, 5) << 1) | ((events_data[i + 7] >> 7) & 0x01);
             event.duration = ((read_uint(events_data + i + 7, 2) & 0x1FFF) << 8) | events_data[i + 9];
             event.running_status = (events_data[i + 10] >> 5) & 0x07;
             event.free_ca_mode = (events_data[i + 10] >> 4) & 0x01;
             event.descriptor_count = events_data[i + 10] & 0x0F;
             
             i += 11;
             
             for (int j = 0; j < event.descriptor_count; j++) {
                 unsigned char *descriptor_data = events_data + i;
                 unsigned char descriptor_tag = descriptor_data[0];
                 unsigned char descriptor_length = descriptor_data[1];
                 
                 if (descriptor_tag == 0x4D) {  // 短事件描述符
                     eit_short_event_t *descriptor = parse_short_event_descriptor(descriptor_data);
                     event.descriptors[j] = descriptor;
                 } else if (descriptor_tag == 0x4E) {  // 扩展事件描述符
                     eit_extended_event_t *descriptor = parse_extended_event_descriptor(descriptor_data);
                     event.descriptors[j] = descriptor;
                 }
                 
                 i += descriptor_length + 2;
             }
         }
     }
     
     // 入口函数
     int main(int argc, char **argv)
     {
         // TODO: 从命令行参数或文件中读取 TS 流数据，并传递给 parse_eit_table 函数进行解析
         
         return 0;
     }
     ```

     





##### [2] 空包

在DVB-TS流中，PID为0x1FFF的包是所谓的Null Packet（空包）。它是一种特殊类型的数据包，其目的是填补传输流中的空白以确保传输速率的一致性。因为Null Packet不包含任何实际的媒体数据，所以它们通常被称为填充包或空包。

Null Packet 由188字节组成，与其他数据包相同，但其有效负载为空。 Null Packet可用于两个方面：

- 填充媒体数据：在DVB-T传输中，Null Packet有时用于填充视频和音频数据之间的空白。如果没有填充数据，可能会导致传输速率变化或丢失部分数据。
- 表示流结束：当使用多个PMT（Program Map Table）的时候，**可以通过发送3个连续的Null Packet表示当前传输流结束了。**

总之，PID为0x1FFF的数据包是一种特殊类型的数据包，在DVB-TS流中用于传输媒体数据的填充或表示传输流的结束。



##### [3] 扩展字段

1. Adaptation Field Control：这里说的很清楚，当该值为00时无视，为01时表示只有载荷数据（中间通过一个0x00分割），为10时只有Adaptation Field，当为11时Adaptation Field后紧跟着Payload（中间没有0x00分割）

2. Adaptation Field：紧跟着4个字节的固定头之后，是一个可能存在又可能不存在的扩展头，这个扩展头是否存在由固定头里的`adaptation_field_control`决定，这个扩展头的长度由该区域第一个字节（8bit）决定，如果`adaptation_field_length==0`，那么整个扩展头的长度也就是1个字节，否则，整个扩展头的长度等于（8+length）bit。**打包 TS 流时 `PAT` 和 `PMT` 表是没有adaptation field的，只有当Payload是`PES`时**，才会有Adaption Field，**不够的长度直接补 0xff 即可**。视频流和音频流都需要加 `adaptation field`，通常加在一个帧的第一个 TS 包和最后一个 TS 包里，中间的 TS 包不加。

##### [4] pcr和opcr的含义

1. 在DVB-TS（数字视频广播传输系统）中，PCR和OPCR是与时间基准有关的参数。它们用于控制视频和音频流的同步。

- PCR (Program Clock Reference)：PCR是一个时间戳，用于在DVB-TS中同步音频和视频流。每个PCR都与特定的PID相关联，并且每个TS分组头中的PCR字段都包含了该PID的PCR值。由于传输过程中可能存在抖动等因素，因此PCR需要经常更新以保持同步。
- OPCR (Original Program Clock Reference)：OPCR是另一种PCR，与PCR具有相同的功能，但与其他元素的时间戳有关。例如，在DVB流中，OPCR通常用于同步PMT（节目映射表）和CAT（条件接入表）等元素。

总之，PCR和OPCR是在DVB-TS中用于控制音频和视频流同步的重要参数。PCR与特定PID相关联，而OPCR则可以用于同步其他TS元素。这些参数通常使用PTS（Presentation Time Stamp）进行更新，以确保媒体数据始终按照正确的时间顺序显示。





- PMT定义如下: 各字段含义如下:

  table_id:8bits的ID,应该是0x02

  section_syntax_indicator:1bit的段语法标志,应该是''1'' ''0'':固定是''0'',如果不是说明数据有错.

  reserved:2bits保留位,应该是''00''

  section_length:16bits段长度,从program_number开始,到CRC_32(包含)的字节总数. program_number:16bits的频道号码,表示当前的PMT关联到的频道.换句话就是说,当前描述的是program_number频道的 信息.

  reserved:2bits保留位,应该是''00''

  version_number:版本号码,如果PMT内容有更新,则version_number会递增1通知解复用程序需要重新接收节目信息,否则 version_number是固定不变的.

  current_next_indicator:当前未来标志符,一般是0

  section_number:当前段号码

  last_section_number:最后段号码,含义和PAT中的对应字段相同,请参考PAT部分. reserved:3bits保留位,一般是''000''.

  PCR_PID:13bits的PCR PID,具体请参考ISO13818-1,解复用程序不使用该参数.

  reserved:4bits保留位,一般是''0000''

  program_info_length:节目信息长度(之后的是N个描述符结构,一般可以忽略掉,这个字段就代表描述符总的长度,单位是Bytes) 紧接着就是频道内部包含的节目类型和对应的PID号码了.

  stream_type:8bits流类型,标志是Video还是Audio还是其他数据.

  reserved:3 bits保留位.

  elementary_PID:13bits对应的数据PID号码(如果stream_type是Video,那么这个PID就是Video PID,如果stream_type标志是Audio,那么这个PID就是Audio PID)

  reserved:4 bits保留位.

  ES_info_length:和program_info_length类似的信息长度(其后是N2个描述符号) CRC_32:32bits段末尾是本段的CRC校验值,一般忽略.

   

  从以上的分析可以看出,只要我们处理了PMT,那么我们就可以获取频道中所有的PID信息,例如当前频道包含多少个Video,共多少个Audio,和其 他数据,还能知道每种数据对应的PID分别是什么. 这样如果我们要选择其中一个Video和Audio收看,那么只需要把要收看的节目的Video PID和Audio PID保存起来,在处理Packet的时候进行过滤即可实现. 比较全面实现解复用的

- 以上伪代码可以实现基本的解复用:检测所有的频道,检测所有stream的PID,选择特定的节目进行播放.只要读取每个Packet的188字节的内 容,然后每次都调用Process_Packet()即可实现简单的解复用. 介绍到这里,我们就可以总结一下DVB搜台的原理了.(好!洗耳恭听!) 机 顶盒先调整高频头到一个固定的频率(如498MHZ),如果此频率有数字信号,则COFDM芯片(如MT352)会自动把TS流数据传送给MPEG- 2 decoder. MPEG-2 decoder先进行数据的同步,也就是等待完整的Packet的到来.然后循环查找是否出现PID== 0x0000的Packet,如果出现了,则马上进入分析PAT的处理,获取了所有的PMT的PID.接着循环查找是否出现PMT,如果发现了,则自动进 入PMT分析,获取该频段所有的频道数据并保存.如果没有发现PAT或者没有发现PMT,说明该频段没有信号,进入下一个频率扫描. 从以上描述可以看出,机顶盒搜索频率是随机发生的,要使每次机顶盒都能搜索到信号,则要求TS流每隔一段时间就发送一 次PAT和PMT.事实上DVB传输系统就是这么做的.因此无论何时接入终端系统,系统都能马上搜索到节目并正确解复用实现播放.不仅仅如此,其他数据也 都是交替传送的.比如第一个Packet可能是PAT,第二个Packet可能是PMT,而第三个Packet可能是Video 1,第四个Packet可能是Video 2, 只要系统传输速度足够快(就是称之为"码率"的东东),实现实时播放是没有任何问题的. 到这里虽然实现了解复用,但可以看出,使用的PID都是枯燥的数字,如果调台要用户自己输入数字那可是太麻烦了,而且还容易输入错 误,操作非常不直观,即使做成一个菜单让用户选择也是非常的呆板.针对这个问题,DVB系统提出了一个SDT表格,该表格标志一个节目的名称,并且能和 PMT中的PID联系起来,这样用户就可以通过直接选择节目名称来选择节目了.



- SDT, Service description section,服务描述段

  SDT可以提供的信息包括:

  (1) 该节目是否在播放中

  (2) 该节目是否被加密

  (3) 该节目的名称

  SDT定义如下: 各字段定义如下:

  table_id:8bits的ID,可以是0x42,表示描述的是当前流的信息,也可以是0x46,表示是其他流的信息(EPG使用此参数)

  section_syntax_indicator:段语法标志,一般是''1''

  reserved_future_used:2bits保留未来使用

  reserved:1bit保留位,防止控制字冲突,一般是''0'',也有可能是''1''

  section_length:12bits的段长度,单位是Bytes,从transport_stream_id开始,到CRC_32结束(包含)

  transport_stream_id:16bits当前描述的流ID，它和nit中的transport_stream_id可以关联起来，我们可以根据SDT获取该业务（频道）的业务列表信息和有线传输信息（位于NIT中）。

  reserved:2bits保留位

  version_number:5bits的版本号码,如果数据更新则此字段递增1

  current_next_indicator:当前未来标志,一般是''0'',表示当前马上使用. original_netword_id:16bits的原始网络ID号

  reserved_future_use:8bits保留未来使用位 接下来是N个节目信息的循环:

  service_id:16 bits的服务器ID,实际上就是PMT段中的program_number. reserved_future_used:6bits保留未来使用位

  EIT_schedule_flag:1bit的EIT信息,1表示当前流实现了该节目的EIT传送 EIT_present_following_flag:1bits的EIT信息,1表示当前流实现了该节目的EIT传送 running_status:3bits的运行状态信息:1-还未播放 2-几分钟后马上开始,3-被暂停播出,4-正在播放,其他---保留

  free_CA_mode:1bits的加密信息,''1''表示该节目被加密. 紧 接着的是描述符,一般是Service descriptor,分析此描述符可以获取servive_id指定的节目的节目名称.具体格式请参考 EN300468中的Service descriptor部分.

   

  分析完毕,则节目名称和节目号码已经联系起来了.机顶盒程序就可以用这些节目名称代替 PID让用户选择,从而实现比较友好的用户界面! 下面参考一下<>中的界面和显示信息. 上 图是<>打开三个不同的码流文件(*.ts)形成的PID信息和节目名称.用户 可以通过切换节目名称的下拉列表框切换节目,也可以通过"视频流"和"音频流"下拉列表框切换Video和Audio!这些数据都是通过分析PAT, PMT和SDT得到的. （转载自网路）







- 1、引言

  在数字电视中，所有视频、音频、文字、图片等经数字化处理后都变成了数据，并按照MPEG-2的标准打包，形成固定长度（188个字节）的传送包，然后将这些数据包进行复用，形成传送码流（TS）。通常由多个节目及业务复用组成的一个TS（TransportStream传输流）流对应一个8MHz带宽的频道。

  数字电视机顶盒中为了找到需要的码流，识别不同的业务信息，在TS流中必须加入一些引导信息，为此，在MPEG-2中，专门定义了PSI（ProgramSpecificInformation）信息，其作用是从一个携带多个节目的某一个TS流中正确找到特定的节目。

  在MPEG-2标准中定义的PSI表，是对单一TS流的描述。由于系统通常存在多个TS流，为了引导数字电视用户能在TS流中快速地找出自己需要的业务，DVB对MPEG-2的PSI进行了扩充，在PSI四个表的基础上再增加了九个表，形成SI（ServiceInformation）。SI是对整个系统所有TS流的描述，在符合MPEG-2(13818-1)的TS传输流中插入DVB标准定义的业务信息（ServiceInformation，SI），使机顶盒(Set-Top-Box)的综合接收解码器（IRD）可以从TS流中提取出节目提供商播出节目的列表和播出参数，以直观的形式显示给数字电视用户，使得用户可以方便地接收、选择数字电视节目。

  PSI表包括节目关联表（PAT）、条件接收表（CAT）、节目映射表（PMT）和网络信息表（NIT）组成，这些表在复用时通过复用器插入到TS流中，并用特定的PID（包标识符）进行标识。SI包括业务描述表（SDT）、事件信息表（EIT）、时间和日期表（TDT）、时间偏移表（TOT）、业务群关联表（BAT）和运行状态表（RST）、填充表（ST）、选择信息表（SIT）、间断信息表（DIT）等表信息。SI中的各表在实际使用中并不都需要传送，其中NIT、SDT、EIT、TDT是必需传送的，其它表则按照需要进行选择传送。

  TS流中有两种标识符，一种是包标识符，一种是表标识符。具有相同PID的不同信息表由表标识符TABLEID来区分，在接收端通过查这些特定的PID来找到它们。

  每个表都有特定的PID值，具体的值如下表： 表 PID值

  本文以有线数字电视中某一个具体TS流配置为准，分析了PSI/SI信息表。图1为PSI/SI全表。   

  图1PSI/SI表

  2、PSI信息的分析

  当机顶盒要要接收某一个指定节目时，PSI表首先从节目关联表（PAT）中取得这个节目的节目映射表（PMT）的PID值，然后从TS流中找出与此PID值相对应的节目映射表（PMT），从这个节目映射表中获得构成这个节目的基本码流的PID值，根据这个PID值滤出相应的视频、音频和数据等基本码流，解码后复原为原始信号，删除含有其余PID的传送包。

  一、PAT表

  要保证TS流能正常接收，在该流中至少有一个完整有效的PAT。节目关联表PAT包括该TS流中的所有节目映射表即每个节目的PMTPID，传输流ID等。

  如图2所示，其中PAT表包标识符PID为0x0000，表标识符（table_id）为0x00，TS流ID（transport?_stream_id）为0x000D即第15个传输流，包含8个节目的PMT信息，分别为0x0061、0x0062、0x0063、0x0064、0x0065、0x0066、0x0067、0x0083。

  由于PSI数据的完整性十分重要，因此在每个PSI段中均需要加CRC的校验码。  

  图2PAT表

  二、PMT表

  节目映射表（PMT）中包括每个节目的基本码流信息即视频信息、音频信息和同密的多家CA的ECM授权控制信息。如图3所示，PMT_PID为0x0063，表标识符（table_id）为0x02,PCRPID为0x1141，视频PID为0x1141，音频（Streamtype0x04）PID为0x1142。其中MPEG-2数字电视13818-2视频部分标准中规定视频基本流类型（Streamtype）为0x02，13818-3音频部分标准中规定音频基本流类型（Streamtype）为0x04。

  PMT表针对节目的加密情况，还含有CA_System_ID用于节目是用何种CA系统加密和ECM_PID用于告知用户如何搜索ECM。CA_System_ID可惟一标识CA系统，分配到用户的智能卡中。以下表中包括同密的三家CA系统CA_System_Id分别是0x491A、0x602、0x606和，对应的ECMPID分别是0x114A、0x114B、0x114C。由于每个节目的加扰参数和加密方式不同，针对不同节目分配不同的ECMPID，所以每个流的每个基本流对应的ECMPID不同。 

  图3 PMT表

  三、CAT表

  CAT表针对具体CA系统中的用户的授权情况，含有标识具体CA系统的CA_System_ID和用于获取授权管理信息EMM的索引EMM_Pid，通过这两项内容就可以获得用户所在CA系统的EMM信息。用户端的条件接收过程就是从读取卡内的CA_System_ID开始的，获取相应的EMM，ECM后，解密解扰的工作就按与发送端相逆的顺序开始了。

  CAT表PID为0x0001，table_id为0x01，其中包括同密的三家CA系统CA_System_ID分别是0x491A、0x602、0x606，对应的EMMPID分别是0x49、0x50、0x51。由于EMM信息通过TS流的方式与其他节目信息复用传输，并且为了用户及时获得授权信息，复用在每一个TS流中，所以在每一个传输流中的EMMPID一样的。

  四、NIT表

  NIT表包括该数字电视网中的所有的传输流的物理传输网信息，包含节目的频道调谐参数、频率、符号率等，这些信息使得接收机可以按照用户的选择以很少的延时或无延时地改变频道、调谐参数，正确地解码出TS。

  其中NIT表PID为0x0010,table_id为0x40，网络ID（network_id）0x01,包括13个传输流，分别传输流ID是0x0001～0x000D，显示当前传输流0x000D中的所有节目ID。以及当前流的传输参数，包括中心频率（frequency）379MHz、数字调制方式（modulator）64QAM、符号率（symbolrate）6.875Msymbol/s。

  3、SI信息的分析

  PSI数据只提供了单个TS的信息，使数字电视机顶盒能对单个TS中的不同节目流进行解码，但它不能提供多个TS的有关业务和节目的类型、什么节目、什么时间开始等信息，因此，DVB系统对PSI进行了扩展，提供了其它不同信息种类的多种表格，形成SI。在实用中，我们将SI所提供的数据通过有序地组织起来，生成类似节目报的形式，它能在电视机上即时浏览，这样将大大方便用户的使用，这就是电子节目指南EPG。

  SI中的常用表为网络信息表（NIT表）、业务描述表（SDT）、事件信息表（EIT）、业务群关联表（BAT）、时间和日期表（TDT）、时间偏移表（TOT）。NIT表在以上PSI表中已描述。

  一、SDT表

  业务描述表（SDT）：它包含了当前传输流和其他传输流的业务信息，比如当前传输流ID，当前传输流中包含的节目名称，节目类型等。

  SDT表PID为0x0011、table_id为0x42、当前传输流ID为0x0d、节目运行状态（running_status）为运行、服务类型（servicetype）为数字电视业务、服务名称（servicename）为七彩戏剧。

  二、EIT表

  事件信息表（EIT）：它包含了与事件或节目相关的数据，EIT是生成EPG的主要表。包括每个节目的当前播出的节目名称、播出开始时间、播出时间段、父母控制级别等信息和下一个播出节目相关信息。

  section-number=0为当前播出事件信息和section-number=1

  为下一个播出事件信息，其中包括事件开始时间（starttime）、播放时间段（duration）、当前播出事件内容（eventname），父母级别控制（parantelratingdescriptor）等。

  三、TDT、TOT、BAT表

  时间和日期表（TDT）：它给出了与当前的时间和日期相关的信息，由于这些信息更新频繁，所以需要单独使用一个表。时间偏移表（TOT）：它给出了与当前时间、日期和本地时间偏移相关的信息，由于这些信息更新频繁，所以需要单独使用一个表。

  业务群关联表（BAT）：它提供了业务群相关的信息，给出了业务群的名称以及每个业务群中的业务列表。分别存在电影、体育和升级程序等三组业务群，其中包括属于该群的节目ID、该节目具体归属的传输流ID和节目类型等信息。利用BAT表的功能，用户根据前端的定义，方便、快捷地搜索到不同业务群的的节目。 

  4、结束语

  通过在数字电视中插入PSI/SI信息，数字电视用户能够方便、快捷地搜索和查询到所有传输的节目名称、节目分类、一周节目内容和具体播放时间等信息，并且随自己的喜好在数字电视接收设备上预定节目，从而实现电子节目指南。





- ***EIT\*****解析**

  PARSINGOF EIT

  "EIT按时间顺序提供每一个业务所包含的事件信息"

  ​    EIT即事件信息表（Event Information Table），它是EPG中绝大部分信息的携带者。事实上，EPG主要就是通过SDT和EIT信息的获取和重组实现的。SDT只提供了频道信息，而EIT则提供各频道下的所有节目的信息。

  ​     EIT的主要信息也是通过插入的描述符来实现的。EIT按照时间顺序提供每一个业务所包含的事件的相关信息（如节目名称、节目简介）。

  | 传输流   | 信息              | table_id  |
  | -------- | ----------------- | --------- |
  | 当前TS流 | 当前/后续事件信息 | 0x4E      |
  | 其他TS流 | 当前/后续事件信息 | 0x4F      |
  | 当前TS流 | 事件时间表信息    | 0x50~0x5F |
  | 其他TS流 | 事件时间表信息    | 0x60~0x6F |

- EIT的表结构分析

  ANALYZEOF THE STRUCTUREOF EIT

   

  ​     EIT表被切分成事件信息段。任何构成EIT的段都由PID=0x0012的TS包传输。下面给出的是事件信息段的结构：

  事件信息段

  Syntax（句法结构）     No. ofbits（所占位数）  Identifier(识别符) Note(注释)

  event_information_section(){

  table_id                      8           uimsbf                         

  Section_syntax_indicator        1             bslbf          通常设为“1”

  Reserved_future_use           1            bslbf

  Reserved                     2            bslbf

  Section_length                12           uimsbf         见注释

  service_id                    16            uimsbf        与PAT中的program_number一致

  Reserved                     2             bslbf

  Version_number               5             uimsbf       见注释

  Current_next_indicator          1             bslbf         见注释

  Section_number               8             uimsbf      见注释

  last_section_number            8             uimsbf      见注释

  transport_stream_id            16             uimsbf      见注释

  original_nerwork_id            16             uimsbf      见注释

  segment_last_section_number   8              uimsbf      见注释

  last_table_id                   8              uimsbf      见注释

  for(i=0;i<N;i++){

    event_id                    16             uimsbf     事件（节目）id

     start_time                   40             bslbf      事件（节目）开始时间

     duration                    24             bslbf      事件（节目）持续始时间

     running_status                3             uimsbf     见注释

    freed_CA_mode              1             bslbf      见注释

    descriptors_loop_length        12           uimsbf

    for(j=0;j<N;j++){

  ​    descriptor()

    }

  }

  CRC_32                        32         rpchof         见注释

  }





- ***TDT\*****解析**

  PARSINGOF TDT

  *"**TDT**仅传送**UTC**时间和日期信息，只有一个段**"*

  ​    TDT为时间和日期表（Time&Date Table），它仅传送UTC时间和日期信息。并且TDT仅包含一个段，其结构如下：

  时间和日期段

  Syntax（句法结构）     No. ofbits（所占位数）  Identifier(识别符) Note(注释) 

  time_date_section(){

  table_id                      8           uimsbf                         

  Section_syntax_indicator        1             bslbf       通常设为“1”

  Reserved_future_use           1            bslbf

  Reserved                     2            bslbf

  Section_length                12           uimsbf         见注释

  UTC_time                      40           bslbf           见注释

  }

  ***UTC_time\***（UTC时间）：40位字段，包含以UTC和MJD形式表示的当前时间和日期。此字段前16位为MJD日期码，后24位按4位BCD编码，表示6个数字。如：93/10/13 12:45:00被编码为“0xC079124500”。





- ***TOT\*****解析**

  PARSINGOF TOT

  "TOT是TDT的一个扩展，增加了一个描述符"

  ​    TOT为时间偏移表（Time Offset Table），它包含了UTC时间和日期信息及当地时间偏移。传输此表的TS包PID为0x0014，table_id=0x73。下面给出了时间偏移段的结构：

  时间偏移段

  Syntax（句法结构）     No. ofbits（所占位数）  Identifier(识别符) Note(注释)

  time_offset_section(){

  table_id                      8           uimsbf                         

  section_syntax_indicator        1             bslbf     通常设为“1”

  reserved_future_use           1            bslbf

  reserved                     2            bslbf

  section_length                12           uimsbf         见注释

  UTC_time                      40           bslbf           见注释

  reserved                     4             bslbf

  descriptors_loop_length        12         uimsbf       

  for(i=0;i<N;i++){

     descriptor()

  }

  CRC_32                     32         rpchof         见注释

  }

   这里的UTC_time和TDT表是一致的，都是以UTC和MJD形式表示当前时间和日期；其格式也与TDT的UTC_time相同，这里不再赘述。需要特别注意的是这里的描述符descriptor()。下面给出了本地时间偏移描述符的结构：

  本地时间偏移描述符

  Syntax（句法结构）  No. of bits（所占位数）Identifier(识别符) 

  local_time_offset_descriptor(){

  descriptor_tag             8      uimsbf    

  descriptor_length         8      uimsbf  

  for(i=0;i<N;i++){

    coutry_code              24       bslbf

    country_region_id          6       bslbf

    reserved                 1       bslbf

    local_time_offset_polarity    1       bslbf

    local_time_offset          16      bslbf

    time_of_change           40      bslbf

    next_time_offset           16      bslbf

  }

  }

  国家代码 country_code

  ​    24位字段，按照ISO 3166用3字符代码指明国家。每个字符根据GB/T 15273.1-1994编码为8位，并依次插入24位字段。假设3个字符代表了一个900至999的数字，那么country_code指定了一组ETSI定义的国家。其分配见ETR 162。国家组的国家代码应该被限制在同一时区内。

  例如：英国由3字符代码“GBR”表示，编码为：“01000111 0100 0010 0101 0010”。

   

  国家区域标识符 country_region_id

  ​    6位字段，表示country_code指明的国家所在的时区。若国家内部里没有时差，则置“000000”。

  | country_region_id | 描述            |
  | ----------------- | --------------- |
  | 00 0000           | 未使用时区扩展  |
  | 00 0001           | 时区1（最东部） |
  | 00 0010           | 时区2           |
  | ……..              | ……..            |
  | 11 1100           | 时区 60         |
  | 11 1101 – 11 1111 | 预留            |

  本地时间偏移极性 local_time_offset_polarity

  ​    1位字段，用于指明随后的local_time_offset的极性。置“0”时，极性为正，说明本地时间早于UTC时间（通常在格林威治以东）；置“1”时，极性为负，说明本地时间晚于UTC时间。

  本地时间偏移 local_time_offset

  ​    16位字段，指出由country_code和country_region_id确定的区域的相对于UTC的时间偏移，范围为-12小时至+13小时。16比特含有4个4位BCD码，顺序为小时的十位，小时的个位，分的十位，分的个位。

  时间变化 time_of_change

  ​    40位字段，指明时间改变时当前的日期（MJD）与时间（UTC），见附录C。该字段分为两部分，前16位给出了LSB格式的日期（MJD），后24位给出了UTC时间（6个4位BCD码）。

  下一时间偏移 next_time_offset

  ​    16位字段，指出由country_code和country_region_id确定的区域，当UTC时间变化时的下一个时间偏移，范围为-12小时至+13小时。此16比特域为4个4位BCD码，依次为时的十位，时的个位，分的十位，分的个位。

<img src="C:\Users\1\AppData\Roaming\Typora\typora-user-images\image-20230625113722304.png"/>