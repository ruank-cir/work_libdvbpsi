# src/ :
该目录中含有使用libdvbpsi库来解析ts流的demo，该demo其实是通过libdvbpsi库的示例程序的dvbinfo来的，基本一摸一样，里面稍微添加了一些注释，里面代码思路大致如下，中间创建了一个子线程，子线程不断把ts流中的数据读取到buffer中，然后把buffer放到fifo的内存池中，然后主程序不断把fifo内存池中的数据获取到，然后来解析,注意该demo中使用你互斥锁和条件变量来控制两个线程的运作，感觉rbq要好一点，
examples/ : 里面写了按照src中的程序自己写的一个demo，只是写了伪代码

# 该demo中你可以获取到以下示例：
	# getopt_long的使用
	# 

# libdvbpsi的使用
1. 调用dvbpsi_new
2. 调用dvbpsi_xxx_attach，需要解析什么表就写什么表
3. 调用dvbpsi_packet_push。然后他会解析数据，然后调用你注册的回调函数，把解析的到信息通过回调函数的结构体给你

# libdvbpsi1.3.3/: 该目录是libdvbpsi源码目录，如果要编译的话，可以使用
1. ./bootstrap
2. ./configure --prefix=/usr --enable-debug --disable-release
3. make install
详情可以查看libdvbpsi1.3.3/INSTALL
